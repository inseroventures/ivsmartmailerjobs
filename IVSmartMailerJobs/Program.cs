﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.Extensions.Configuration;
using Microsoft.EntityFrameworkCore;
using IVSmartMailer.Data;
using IVSmartMailer.Services;
using IVSmartMailer.ViewModels;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Abstractions;
using Microsoft.Extensions.Logging;
using IVSmartMailer.PimDB;

namespace IVSmartMailerJobs
{
    class Program
    {
        public static IConfigurationRoot _config { get; set; }
        private static IVDBContext _context;
        private static DbContextOptionsBuilder<IVDBContext> _optionsBuilder;

        public static IConfiguration Configuration { get; set; }

        private static void WriteLog(string message)
        {
            Console.WriteLine(message);

        }

        private static void WriteResult (List<string> result, List<string> err)
        {
            WriteLog("Results");
            foreach (var st in result)
            {
                WriteLog(st);
            };

            WriteLog("");
            WriteLog("Error Messages");

            foreach (var st in err)
            {
                WriteLog(st);
            };
        }


        static void Main(string[] args)
        {

            WriteLog("Commencing SmartMailerJobs at " + DateTime.Now.ToString("ddd, dd MMM yyyy hh:mm") + " with parameters " + String.Join(",", args));

            var environmentName = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT");

            var xx = Directory.GetCurrentDirectory();

            var builder = new ConfigurationBuilder()
              .SetBasePath(Directory.GetCurrentDirectory())
              .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)
             .AddJsonFile($"appsettings.{environmentName}.json", optional: true)
             .AddEnvironmentVariables();

            Configuration = builder.Build();

            var sqlstr = Configuration.GetConnectionString("DefaultConnection");

   //         _optionsBuilder = new DbContextOptionsBuilder<IVDBContext>();
   //         _optionsBuilder.UseSqlServer(sqlstr);

   //         _context = new IVDBContext(_optionsBuilder.Options);

            var serviceProvider = new ServiceCollection()
              .AddLogging()
               .AddSingleton<IConfiguration>(Configuration)
              .AddSingleton<IImportOrders, ImportOrders>()
              .AddSingleton<IIVRepository, IVRepository>()
              .AddSingleton<IOrdersRepository, OrdersRepository>()
              .AddSingleton<IShopify, Shopify>()
              .AddSingleton<ICalculateFavourites, CalculateFavourites>()
              .AddSingleton<ICustProdTypeFreq, CustProdTypeFreq>()
              .AddSingleton<IGetEscCards, GetEscCards>()
              .AddSingleton<IResetPriceBands, ResetPriceBands>()
              .AddSingleton<IImportProducts, ImportProducts>()
              .AddSingleton<IPimRepository, PimRepository>()
              .AddAutoMapper(cfg =>
              {
                  cfg.AddProfile<AutomapperProfile>();
              })
              .AddDbContext<IVDBContext>(options =>
                    options.UseSqlServer(Configuration.GetConnectionString("DefaultConnection")))
              .AddDbContext<PimDBContext>(options =>
                    options.UseSqlServer(Configuration.GetConnectionString("PimDBConnection")))
              .BuildServiceProvider();




            var oRepository = serviceProvider.GetService<IOrdersRepository>();
            var importOrders = serviceProvider.GetService<IImportOrders>();
            var importProducts = serviceProvider.GetService<IImportProducts>();
            var getEscCards = serviceProvider.GetService<IGetEscCards>();
            var custProdTypeFreq = serviceProvider.GetService<ICustProdTypeFreq>();
            var resetPriceBands = serviceProvider.GetService<IResetPriceBands>();
            var calcFavourites = serviceProvider.GetService<ICalculateFavourites>();

            ILoggerFactory logging = serviceProvider.GetService<ILoggerFactory>();
            logging.AddConsole(Configuration.GetSection("Logging"));

     //       bool mustRun = true;



            if (args.Length > 0 && !(args[0] == "CalcFavourites" && args.Length == 1))
            {
                if (args[0] == "ImportOrders")
                {
                    OrderInputViewModel vm = new OrderInputViewModel
                    {
                        LastOrderNo = oRepository.GetLastOrderNo(),
                        LastUpdate = oRepository.GetLastUpdate()

                    };

                    vm.LastShopifyId = oRepository.GetShopifyId((int)vm.LastOrderNo);

                    var x = importOrders.ImportShopifyOrders(vm).Result;

                    WriteLog("");
                    WriteLog("Error Messages");

                    foreach (var st in vm.ErrorMessages)
                    {
                        WriteLog(st);
                    };


                }
                else if (args[0] == "ImportProducts")
                {
                    WriteLog("Commencing Import Products");
                    DateTime now = DateTime.Now;

                    var mod = new ProductImportViewModel
                    {
                        ErrorMessages = new List<string>(),
                        ResultSummary = new List<string>(),
                        Now = true,
                        To = now,
                        From = new DateTime(now.Year, 01, 01),
                        Force = false,
                        FromId = null
                    };

                    var res = importProducts.ImportPimProducts(mod);

                    WriteResult(res.ResultSummary, res.ErrorMessages);


                }
                else if (args[0] == "GetShopifySOEHandles")
                {
                    WriteLog("Commencing Shopify SOE Handles");

                    var mod = new GetShopSOEHandlesVM
                    {
                        ErrorMessages = new List<string>(),
                        ResultSummary = new List<string>(),
                    };


                    GetShopSOEHandlesVM res = importProducts.GetShopSoeHandles(mod).Result;

                    WriteResult(res.ResultSummary, res.ErrorMessages);

                }
                else if (args[0] == "GetEscCards")
                {
                    WriteLog("Commencing Get Escape Cards");

                    var mod = new GetEscCardsViewModel
                    {
                        ErrorMessages = new List<string>(),
                        ResultSummary = new List<string>(),
                        Customer = "",
                        FromId = null
                    };

                    GetEscCardsViewModel res = getEscCards.ImportShopifyEscCardsAsync(mod).Result;

                    WriteResult(res.ResultSummary, res.ErrorMessages);

                }
                else if (args[0] == "UpdCustProdTypeFreq")
                {
                    WriteLog("Commencing Update Customer Product Type Frequency");

                    var mod = new UpdateProdTypeFreqViewModel
                    {
                        ErrorMessages = new List<string>(),
                        ResultSummary = new List<string>(),
                        Customer = "",
                        FromId = null
                    };

                    UpdateProdTypeFreqViewModel res = custProdTypeFreq.ImportCustProdTypeFreqAsync(mod).Result;

                    WriteResult(res.ResultSummary, res.ErrorMessages);

                }
                else if (args[0] == "ResetProdTypePriceBands")
                {

                    WriteLog("Commencing Reset Product Type Price Bands");

                    var mod = new ResetPriceBandsVM
                    {
                        ErrorMessages = new List<string>(),
                        ResultSummary = new List<string>(),
                        ProductTitle = "",
                        ProductType = null
                    };

                    ResetPriceBandsVM res = resetPriceBands.Reset(mod).Result;

                    WriteResult(res.ResultSummary, res.ErrorMessages);


                }
                else if (args[0] == "CalcFavourites" && args[1] == "CustomerColour")
                {
                    WriteLog("Commencing Calculate Customer Favourite Colours");

                    List<string> ErrorMess = new List<string>();
                    List<string> ResultSummary = new List<string>();

                    calcFavourites.AllCustomerAndColour(0, ErrorMess, ResultSummary);
                    WriteResult(ResultSummary, ErrorMess);


                }
                else if (args[0] == "CalcFavourites" && args[1] == "CustomerBrands")
                {
                    WriteLog("Commencing Calculate Customer Favourite Brands");

                    List<string> ErrorMess = new List<string>();
                    List<string> ResultSummary = new List<string>();

                    calcFavourites.AllCustomerAndBrands(0, ErrorMess, ResultSummary);
                    WriteResult(ResultSummary, ErrorMess);
                }
                else if (args[0] == "CalcFavourites" && args[1] == "CustomerPriceBands")
                {
                    WriteLog("Commencing Calculate Customer Favourite Price Bands");

                    List<string> ErrorMess = new List<string>();
                    List<string> ResultSummary = new List<string>();

                    calcFavourites.AllCustomerAndPriceBand(0, ErrorMess, ResultSummary);
                    WriteResult(ResultSummary, ErrorMess);
                }
                else if (args[0] == "CalcFavourites" && args[1] == "CustomerDiscipline")
                {
                    WriteLog("Commencing Calculate Customer Favourite Disciplines");

                    List<string> ErrorMess = new List<string>();
                    List<string> ResultSummary = new List<string>();

                    calcFavourites.AllCustomerAndDisc(0, ErrorMess, ResultSummary);
                    WriteResult(ResultSummary, ErrorMess);
                }
                else if (args[0] == "CalcFavourites" && args[1] == "CustomerGender")
                {
                    WriteLog("Commencing Calculate Customer Gender from Purchase");

                    List<string> ErrorMess = new List<string>();
                    List<string> ResultSummary = new List<string>();

                    calcFavourites.AllCustomerAndGender(0, ErrorMess, ResultSummary);
                    WriteResult(ResultSummary, ErrorMess);
                }
                else if (args[0] == "CalcFavourites" && args[1] == "ProductTypeColours")
                {
                    WriteLog("Commencing Calculate Product Type Favourite Colours");

                    List<string> ErrorMess = new List<string>();
                    List<string> ResultSummary = new List<string>();

                    calcFavourites.AllProductTypeAndColour(0, ErrorMess, ResultSummary);
                    WriteResult(ResultSummary, ErrorMess);
                }
                else if (args[0] == "CalcFavourites" && args[1] == "ProductCategoryColours")
                {
                    WriteLog("Commencing Calculate Product Category Favourite Colours");

                    List<string> ErrorMess = new List<string>();
                    List<string> ResultSummary = new List<string>();

                    calcFavourites.AllCategoryTypeAndColour(0, ErrorMess, ResultSummary);
                    WriteResult(ResultSummary, ErrorMess);
                }
                else if (args[0] == "CalcFavourites" && args[1] == "BrandColours")
                {
                    WriteLog("Commencing Calculate Brand Favourite Colours");

                    List<string> ErrorMess = new List<string>();
                    List<string> ResultSummary = new List<string>();

                    calcFavourites.AllBrandAndColour(0, ErrorMess, ResultSummary);
                    WriteResult(ResultSummary, ErrorMess);

                }
                else if (args[0] == "CalcFavourites" && args[1] == "Brands")
                {
                    WriteLog("Commencing Calculate Favourite Brands for Store");

                    List<string> ErrorMess = new List<string>();
                    List<string> ResultSummary = new List<string>();

                    calcFavourites.AllBrands(0, ErrorMess, ResultSummary);
                    WriteResult(ResultSummary, ErrorMess);
                }
                else if (args[0] == "CalcFavourites" && args[1] == "Disciplines")
                {
                    WriteLog("Commencing Calculate Favourite Disciplines for Store");

                    List<string> ErrorMess = new List<string>();
                    List<string> ResultSummary = new List<string>();

                    calcFavourites.AllDisciplines(0, ErrorMess, ResultSummary);
                    WriteResult(ResultSummary, ErrorMess);

                }
            }
            else
            {
                WriteLog("No arguement specified - no action taken");
            }

            WriteLog("Completed");

             
        }
    }
}
